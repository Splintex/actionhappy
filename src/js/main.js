//= ../../bower_components/jquery/dist/jquery.min.js

//= ../../bower_components/Chart.js/Chart.min.js

//= partials/head.js
//= partials/materialize/jquery.easing.1.3.js
//= partials/materialize/global.js
//= partials/materialize/animation.js
//= partials/materialize/velocity.min.js
//= partials/materialize/buttons.js
//= partials/materialize/collapsible.js
//= partials/materialize/dropdown.js
//= partials/materialize/leanModal.js
//= partials/materialize/parallax.js
//= partials/materialize/tabs.js
//= partials/materialize/forms.js

//= partials/materialize/transitions.js
//= partials/materialize/waves.js
//= partials/jquery.particleground.min.js

//= app.js